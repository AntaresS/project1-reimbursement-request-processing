# Project1-Reimbursement Request Processing

# Project Description
    -Simple full stack application, simulating an employee reimbursement resolution system. 

# Technologies Used
    Technologies:
        -Java version 1.8
        -Javalin version 4.0.1
        -PostgreSQL version 9
        -JUnit version 4.12
        -log4j version 2.13.3
        -HTML/CSS/Javascript
        -AJAX
        -HTML/CSS/Javascript
        -AJAX
        -Maven

    Environment:
        -Jetty
        -IntelliJ

# Features
    -View personnel data and reimbursement requests
    -Filter various records by unique employee identification numbers
    -Resolve individual reimbursement requests and view updated record

    TO DO List:
        -develop more appealing ui/ux features
        -add client-side data validation features
        -improve backend access to permit scalability

# Getting Started
    -Begin by cloning the git repository to the local folder of your choice
        git clone https://gitlab.com/AntaresS/project1-reimbursement-request-processing.git
    
    -create and switch to a new a new branch
        git branch <your branch name>
        git checkout <your branch name>
    
    -open the project in IntelliJ or your IDE of choice (refer to IDE documentation for assistance importing a Maven project from IntelliJ) 
    