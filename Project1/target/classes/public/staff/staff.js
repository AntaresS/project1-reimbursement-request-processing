class Update {
    constructor(employeeID, firstName, lastName, streetAddress, phoneNumber, emailAddress) {
        this.employeeID = employeeID || "";
        this.firstName = firstName || "";
        this.lastName = lastName || "";
        this.streetAddress = streetAddress || "";
        this.phoneNumber = phoneNumber || "";
        this.emailAddress = emailAddress || ""; 
    }
}

class reimbursementRequest {
    constructor(amount, description, submitterID) {
        this.amount = amount || "",
        this.description = description || "",
        this.submitterID = submitterID || ""
    }
}

const getEmployeeByID = (employeeID) => {
    return fetch(`http://localhost:7000/api/employees/${employeeID}`);
}

const getReimbursementReqs = (employeeID) => {
    return fetch(`http://localhost:7000/api/requests/${employeeID}`);
}

const saveReimbursementReq = (request) => {
    return fetch("http://localhost:7000/api/requests", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
    });
}

const updateEmployee = (update) => {
    return fetch(`http://localhost:7000/api/employees/${update.employeeID}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(update)
    });
}

document.addEventListener('DOMContentLoaded', () => {
    const resetButton = document.querySelector("#resetButton");
    const getEmpButton = document.querySelector("#getEmployee");
    const saveReimButton = document.querySelector("#saveReimButton");
    const updateButton = document.querySelector("#updateSubmit")
    const getPendingButton = document.querySelector("#pending");
    const getResolvedButton = document.querySelector("#resolved");
    const logoutButton = document.querySelector("#logoutButton");
    
    const requestForm = document.forms[1];
    const updateForm = document.forms[3];
    
    const idField = document.querySelector("#idField");
    const requestorID = document.querySelector("#requestorID");

    resetButton.addEventListener('click', (event) => {
        event.preventDefault();
        console.log(event);
    });

    getPendingButton.addEventListener('click', (event) => {
        event.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID']
        
        getReimbursementReqs(requestorID.value)
            .then((resp) => resp.json())
            .then((json) => {
                const reqList = json;
                const allBody = document.querySelector("#allBody");
                reqList.forEach((element) => {
                    if(element.status.toLowerCase() === "pending"){
                        const reqRecord = document.createElement("tr");
                        fields.forEach((field) => {
                            const tableData = document.createElement("td");
                            tableData.textContent = element[field];
                            reqRecord.appendChild(tableData);
                        })
                        allBody.append(reqRecord);
                    }
                });
            });
    });

    getResolvedButton.addEventListener('click', (event) => {
        event.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID','resolverID']

        getReimbursementReqs(requestorID.value)
            .then((resp) => resp.json())
            .then((json) => {
                const reqList = json;
                const allBody = document.querySelector("#allBody");
                reqList.forEach((element) => {
                    if(element.status.toLowerCase() !== "pending"){
                        const reqRecord = document.createElement("tr");
                        fields.forEach((field) => {
                            const tableData = document.createElement("td");
                            tableData.textContent = element[field];
                            reqRecord.appendChild(tableData);
                        })
                        allBody.append(reqRecord);
                    }
                });
            });
    });

    getEmpButton.addEventListener('click', (event) => {
        event.preventDefault();
        const fields = ['employeeID','firstName','lastName','streetAddress','phoneNumber','position','emailAddress']

        getEmployeeByID(idField.value)
            .then((resp) => resp.json())
            .then((json) => {
                const empFile = json;
                const singleBody = document.querySelector("#singleBody");
                const empRecord = document.createElement("tr");
                fields.forEach((field) => {
                    const tableData = document.createElement("td");
                    tableData.textContent = empFile[field];
                    empRecord.appendChild(tableData);
                });
                singleBody.append(empRecord);
            });
    });


    saveReimButton.addEventListener('click', (event) => {
        event.preventDefault();
        const { amount, description, submitterID } = requestForm.elements;
        const newReq = new reimbursementRequest(amount.value, description.value, submitterID.value);

        saveReimbursementReq(newReq)
            .then((resp) => resp.json())
            .then((json) => {
                const message = json;
                const viewNewReqID = document.querySelector("#viewNewRequestID");
                const newReqMessage = document.createElement("h3");
                newReqMessage.textContent = `New Request ID: ${JSON.stringify(message)}`;
                viewNewReqID.append(newReqMessage); 
            });

    });

    updateButton.addEventListener('click', (event) => {
        event.preventDefault();
        const {employeeID, firstName, lastName, streetAddress, phoneNumber, emailAddress} = updateForm.elements;
        const update = new Update(employeeID.value, firstName.value, lastName.value, streetAddress.value, phoneNumber.value, emailAddress.value);
        
        const fields = ['employeeID','firstName','lastName','streetAddress','phoneNumber','position','emailAddress']
        updateEmployee(update)
            .then((resp) => resp.json())
            .then((json) => {
                const updRecord = json;
                console.log(updRecord);
                const updateBody = document.querySelector("#updateBody");
                const empRecord = document.createElement("tr");
                fields.forEach((field) => {
                    const tableData = document.createElement("td");
                    tableData.textContent = updRecord[field];
                    empRecord.appendChild(tableData);
                });
                updateBody.append(empRecord);
            });
    });

    logoutButton.addEventListener('click', (e) => {
        sessionStorage.clear();
    });
    
});