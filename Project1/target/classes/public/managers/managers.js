class Employee {
    constructor(firstName,lastName,streetAddress,phoneNumber,position,password,emailAddress) {
        this.firstName = firstName || "";
        this.lastName = lastName || "";
        this.streetAddress = streetAddress || "";
        this.phoneNumber = phoneNumber || "";
        this.position = position;
        this.password = password;
        this.emailAddress = emailAddress;
    }
}

const getAllEmployees = () => {
    return fetch("http://localhost:7000/api/employees");
}

const getPendingRequests = () => {
    return fetch("http://localhost:7000/api/requests/pending");
}

const getResolvedRequests = () => {
    return fetch("http://localhost:7000/api/requests/resolved");
}

const getSingleEmployeeRequest = (employeeID) => {
    return fetch(`http://localhost:7000/api/requests/${employeeID}`);
}

const submitEmployee = (employee) => {
    return fetch("http://localhost:7000/api/employees", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(employee)
    });
}

const  updateRequest = (status) => {
    const reqID = document.querySelector("#requestID");
    const mgrID = document.querySelector("#managerID");

    let update = {
        'requestID': reqID.value,
        'managerID': mgrID.value,
        'status': status
    };
    
    return fetch(`http://localhost:7000/api/requests/${reqID.value}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(update)
    });
}



document.addEventListener('DOMContentLoaded', (e) => {
    const getIndividualButton = document.querySelector("#getIndividualReqs");
    const pendingButton = document.querySelector("#getAllPending");
    const resolvedButton = document.querySelector("#getAllResolved");
    const approveButton = document.querySelector("#approval");
    const denyButton = document.querySelector("#denial");
    const getAllButton =  document.querySelector("#getAllEmployees");
    const logoutButton = document.querySelector("#logoutButton");
    

    getAllButton.addEventListener('click', (e) => {
        e.preventDefault();
        const fields = ['employeeID','firstName','lastName','streetAddress','phoneNumber','position','emailAddress']
        getAllEmployees()
            .then((resp) => resp.json())
            .then((json) => {
                const employeeList = json;
                const allBody = document.querySelector("#allBody");
                employeeList.forEach((element) => {
                    const empRecord = document.createElement("tr");
                    fields.forEach((field) => {
                        const tableData = document.createElement("td");
                        tableData.textContent = element[field];
                        empRecord.appendChild(tableData);
                    })
                    allBody.append(empRecord);
            });
        });
    });

    pendingButton.addEventListener('click', (e) => {
        e.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID']
        getPendingRequests()
            .then((resp) => resp.json())
            .then((json) => {
                const requestList = json;
                const pendingBody = document.querySelector("#pendingBody");
                requestList.forEach((element) => {
                    const reqRecord = document.createElement("tr");
                    fields.forEach((field) => {
                        const tableData = document.createElement("td");
                        tableData.textContent = element[field];
                        reqRecord.appendChild(tableData);
                    })
                    pendingBody.append(reqRecord);
            });
        });
    });

    resolvedButton.addEventListener('click', (e) => {
        e.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID','resolverID'];
        getResolvedRequests()
            .then((resp) => resp.json())
            .then((json) => {
                const requestList = json;
                const rslvdBody = document.querySelector("#rslvdBody");
                requestList.forEach((element) => {
                    const reqRecord = document.createElement("tr");
                    fields.forEach((field) => {
                        const tableData = document.createElement("td");
                        tableData.textContent = element[field];
                        reqRecord.appendChild(tableData);
                    });
                    rslvdBody.append(reqRecord);
            });
        });
    });
    

    getIndividualButton.addEventListener('click', (e) => {
        e.preventDefault();
        
        const empID  = document.querySelector("#individualID").value;
        const fields = ['requestID','amount','description','status','submitterID','resolverID'];
        
        getSingleEmployeeRequest(empID)
            .then((resp) => resp.json())
            .then((json) => {
                const empReqList = json;
                const singleBody = document.querySelector("#singleBody");
                empReqList.forEach((element) => {
                    const empRecord = document.createElement("tr");
                    fields.forEach((field) => {
                        const tableData = document.createElement("td");
                        tableData.textContent = element[field];
                        empRecord.appendChild(tableData);
                    });
                    singleBody.append(empRecord);
                });
            });

    });

    approveButton.addEventListener('click', (e) => {
        e.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID','resolverID'];
        updateRequest(approveButton.value)
            .then(resp => resp.json())
            .then(json => {
                const rslvdRequest = json;                
                const resolvedBody = document.querySelector("#resolvedBody");
                const rslvRecord = document.createElement("tr");
                fields.forEach((field) => {
                    const tableData = document.createElement("td");
                    tableData.textContent = rslvdRequest[field];
                    rslvRecord.appendChild(tableData);
                });
                resolvedBody.append(rslvRecord);
            });
    });
    
    denyButton.addEventListener('click', (e) => {
        e.preventDefault();
        const fields = ['requestID','amount','description','status','submitterID','resolverID'];
        updateRequest(denyButton.value)
            .then((resp) => resp.json())
            .then((json) => {
                const rslvdRequest = json;            
                const resolvedBody = document.querySelector("#resolvedBody");
                const rslvRecord = document.createElement("tr");
                fields.forEach((field) => {
                    const tableData = document.createElement("td");
                    tableData.textContent = rslvdRequest[field];
                    rslvRecord.appendChild(tableData);
                });
                resolvedBody.append(rslvRecord);
            });
    });

    logoutButton.addEventListener('click', (e) => {
        sessionStorage.clear();
    });
    
 });