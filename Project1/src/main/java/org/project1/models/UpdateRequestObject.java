package org.project1.models;

/**
 * created a unique update object to help the interaction between Manager and Employee positions in SQL,
 * which are both eventually required to resolve reimbursement requests
 * could have, perhaps, achieved the same thing by using a type of join
 */
public class UpdateRequestObject {
    private int requestID;
    private int managerID;
    private String status;

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getManagerID() {
        return managerID;
    }

    public void setManagerID(int managerID) {
        this.managerID = managerID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UpdateRequestObject{" +
                "requestID=" + requestID +
                ", managerID=" + managerID +
                ", status='" + status + '\'' +
                '}';
    }
}
