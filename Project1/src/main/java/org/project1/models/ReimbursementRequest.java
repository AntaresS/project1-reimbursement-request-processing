package org.project1.models;

/**
 * it would be more realistic to include the date in a no-arg constructor
 * the inclusion of both submitter and resolver IDs suggested the creation of the UpdateRequestObject
 */
public class ReimbursementRequest {
    private int requestID;
    private double amount;
    private String description;
    private String status;
    private int submitterID;
    private int resolverID;

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSubmitterID() {
        return submitterID;
    }

    public void setSubmitterID(int submitterID) {
        this.submitterID = submitterID;
    }

    public int getResolverID() {
        return resolverID;
    }

    public void setResolverID(int resolverID) {
        this.resolverID = resolverID;
    }

    @Override
    public String toString() {
        return "ReimbursementRequest{" +
                "requestID=" + requestID +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", submitterID=" + submitterID +
                ", resolverID=" + resolverID +
                '}';
    }
}
