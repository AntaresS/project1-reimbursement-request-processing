package org.project1.models;

import java.util.Objects;

/**
 * used for back-end logic to verify login credentials, the currentEmployee boolean is not really being used since everyone is set to 'true'
 * right now. future logic could be added for additional HR features. If, for example, a former employee had an open re-
 * imbursement request, this field would be very useful.
 */
public class Login {
    private int employeeID;
    private String password;
    private String position;
    private boolean currentEmployee;

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isCurrentEmployee() {
        return currentEmployee;
    }

    public void setCurrentEmployee(boolean currentEmployee) {
        this.currentEmployee = currentEmployee;
    }

    @Override
    public String toString() {
        return "Login{" +
                "employeeID=" + employeeID +
                ", password='" + password + '\'' +
                ", position='" + position + '\'' +
                ", currentlyEmployee=" + currentEmployee +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Login login = (Login) o;
        return employeeID == login.employeeID && currentEmployee == login.currentEmployee && password.equals(login.password) && position.equals(login.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeID, password, position, currentEmployee);
    }
}
