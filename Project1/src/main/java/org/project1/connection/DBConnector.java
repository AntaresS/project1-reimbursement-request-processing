package org.project1.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * same JDBC connector we've been using since project0. i should look into building a better version of this
 */
public class DBConnector {
    private String username;
    private String password;
    private String url;

    private static Logger logger = LogManager.getLogger(DBConnector.class);

    private static Properties props = new Properties();

    static {
        try {
            //read config from db.properties
            props.load(ClassLoader.getSystemClassLoader().getResourceAsStream("db.properties"));
            //load the class for instantiation
            Class genConnector = Class.forName(props.getProperty("db.driver_name"));
            //create an instance of the driver using reflection
            Driver driver = (Driver) genConnector.newInstance();
            //register the driver with the Drivermanager
            DriverManager.registerDriver(driver);
        } catch (IOException e) {
            logger.error("IOexception thrown building JDBC connection");
            e.printStackTrace();
            throw new RuntimeException("could not parse db.properties", e);
        } catch (ClassNotFoundException e) {
            logger.error("could not load JDBC drivers");
            e.printStackTrace();
            throw new RuntimeException("could not load JDBC drivers");
        } catch (InstantiationException e) {
            logger.error("Unable to create an instance of the JDBC driver class");
            e.printStackTrace();
            throw new RuntimeException("could not create instance of JDBC driver class");
        } catch (IllegalAccessException e) {
            logger.error("driver constructor is marked private");
            e.printStackTrace();
            throw new RuntimeException("driver constructor is marked as private");
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while building connection");
            throwables.printStackTrace();
        }
    }

    //instance block
    {
        username = props.getProperty("db.username");
        password = props.getProperty("db.password");
        url = props.getProperty("db.url");
    }

    public DBConnector(){

    }
    //figure out why this is built this way and if there's another way to do it

    public Connection newConnection(String user, String pass, String link) throws SQLException {
        return DriverManager.getConnection(link, user, pass);
    }

    public Connection newConnection() throws SQLException {
        return newConnection(username, password, url);
    }
}
