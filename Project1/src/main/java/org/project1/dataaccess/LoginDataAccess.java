package org.project1.dataaccess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.models.Login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * single POST operation for user login, returns a Login object
 */
public class LoginDataAccess {
    private DBConnector dbconnect;
    Logger logger = LogManager.getLogger(LoginDataAccess.class);

    public LoginDataAccess(DBConnector connector) {
        dbconnect = connector;
    }

    public Login pageRedirectCredentials(int employeeID, String password) {
        logger.trace("New login attempt recorded");
        Login logReturn = new Login();
        try(Connection cnxn = dbconnect.newConnection()){
            String sql = "select employee_id, employee_role, currently_employed, employee_password from employee_data where employee_id = ? and employee_password = ?";
            PreparedStatement ps = cnxn.prepareStatement(sql);
            ps.setInt(1,employeeID);
            ps.setString(2,password);

            ResultSet results = ps.executeQuery();

            while(results.next()) {

                logReturn.setEmployeeID(results.getInt("employee_id"));
                logReturn.setPosition(results.getString("employee_role"));
                logReturn.setCurrentEmployee(results.getBoolean("currently_employed"));
                logReturn.setPassword(results.getString("employee_password"));

            }

        } catch (SQLException throwables) {
            logger.error("SQL exception thrown at login");
            throwables.printStackTrace();
        }
        return logReturn;
    }
}
