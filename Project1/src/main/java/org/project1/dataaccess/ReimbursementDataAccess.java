package org.project1.dataaccess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.models.ReimbursementRequest;
import org.project1.models.UpdateRequestObject;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;

/**
 * CRUD operations for the reimbursement requests
 */
public class ReimbursementDataAccess {
    Logger logger = LogManager.getLogger(ReimbursementDataAccess.class);
    private final DBConnector dbconnect;

    public ReimbursementDataAccess(DBConnector connector) { dbconnect = connector; }

    public int saveReimbursementReq(ReimbursementRequest reimObj) {
        logger.trace("saving a new employee");
        int newReqID = 0;
        try(Connection cnxn = dbconnect.newConnection()) {
            if(reimObj.getSubmitterID() == 0){
                throw new RuntimeException("Sumbitter ID cannot be zero");
            }
            String sql = "insert into reimbursement_request(amount, description, status, submitter_id) values (?,?,?,?)";
            PreparedStatement ps = cnxn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setDouble(1, reimObj.getAmount());
            ps.setString(2, reimObj.getDescription());
            ps.setString(3,"pending");
            ps.setInt(4,reimObj.getSubmitterID());

            DatabaseMetaData dbMeta = cnxn.getMetaData();
            if(dbMeta.supportsTransactionIsolationLevel(Connection.TRANSACTION_SERIALIZABLE)) {
                cnxn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

                ps.executeUpdate();

                ResultSet results = ps.getGeneratedKeys();

                while(results.next()) {
                    newReqID = results.getInt("request_id");
                    System.out.println(newReqID);
                }
            } else {
                throw new RuntimeException("Cannot complete transaction");
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while saving an employee object");
            throwables.printStackTrace();
        }
        return newReqID;
    }
    public List<ReimbursementRequest> getPendingRequests() {
        logger.trace("recovering 'pending' requests");
        List<ReimbursementRequest> reqList = new ArrayList<>();

        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "select request_id, amount, description, status, submitter_id from reimbursement_request where status = ?";
            PreparedStatement ps  = cnxn.prepareStatement(sql);
            ps.setString(1, "pending");
            ResultSet results = ps.executeQuery();

            while(results.next()){
                ReimbursementRequest request = new ReimbursementRequest();

                request.setRequestID(results.getInt("request_id"));
                request.setAmount(results.getDouble(("amount")));
                request.setDescription(results.getString("description"));
                request.setStatus(results.getString("status"));
                request.setSubmitterID(results.getInt("submitter_id"));

                reqList.add(request);
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while recovering pending requests");
            throwables.printStackTrace();
        }
        return reqList;
    }

    public List<ReimbursementRequest> getResolvedRequests() {
        logger.trace("recovering resolved requests");
        List<ReimbursementRequest> reqList = new ArrayList<>();

        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "select request_id, amount, description, status, submitter_id, resolver_id from reimbursement_request where status != ?";
            PreparedStatement ps  = cnxn.prepareStatement(sql);
            ps.setString(1, "pending");
            ResultSet results = ps.executeQuery();

            while(results.next()){
                ReimbursementRequest request = new ReimbursementRequest();

                request.setRequestID(results.getInt("request_id"));
                request.setAmount(results.getDouble(("amount")));
                request.setDescription(results.getString("description"));
                request.setStatus(results.getString("status"));
                request.setSubmitterID(results.getInt("submitter_id"));
                request.setResolverID(results.getInt("resolver_id"));

                reqList.add(request);
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while recovering resolved requests");
            throwables.printStackTrace();
        }
        return reqList;
    }

    public List<ReimbursementRequest> getRequestsByEmployeeID(int empID) {
        logger.trace("recovering specific employee requests");
        List<ReimbursementRequest> reqList = new ArrayList<>();

        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "select request_id, amount, description, status, submitter_id, resolver_id from reimbursement_request where submitter_id = ?";
            PreparedStatement ps  = cnxn.prepareStatement(sql);
            ps.setInt(1, empID);
            ResultSet results = ps.executeQuery();

            while(results.next()){
                ReimbursementRequest request = new ReimbursementRequest();

                request.setRequestID(results.getInt("request_id"));
                request.setAmount(results.getDouble(("amount")));
                request.setDescription(results.getString("description"));
                request.setStatus(results.getString("status"));
                request.setSubmitterID(results.getInt("submitter_id"));
                request.setResolverID(results.getInt("resolver_id"));

                reqList.add(request);
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while recovering employee requests");
            throwables.printStackTrace();
        }
        return reqList;
    }

    public ReimbursementRequest getRequestByID(UpdateRequestObject updObj) {
        logger.trace("Recover request by the request ID");
        ReimbursementRequest returnReq = new ReimbursementRequest();
        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "select request_id, amount, description, status, submitter_id, resolver_id from reimbursement_request where request_id = ?";
            PreparedStatement ps = cnxn.prepareStatement(sql);
            ps.setInt(1,updObj.getRequestID());

            ResultSet results = ps.executeQuery();

            while(results.next()) {
                returnReq.setRequestID(results.getInt("request_id"));
                returnReq.setAmount(results.getDouble("amount"));
                returnReq.setDescription(results.getString("description"));
                returnReq.setStatus(results.getString("status"));
                returnReq.setSubmitterID(results.getInt("submitter_id"));
                returnReq.setResolverID(results.getInt("resolver_id"));
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while recovering employee request");
            throwables.printStackTrace();
        }
        return returnReq;
    }

    public void updateRequestByID(UpdateRequestObject updObj) {
        logger.trace("updating a specific employee");
        int requestID = 0;
        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "update reimbursement_request set status = ?, resolver_id = ? where request_id = ?";
            PreparedStatement ps  = cnxn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, updObj.getStatus());
            ps.setInt(2,updObj.getManagerID());
            ps.setInt(3, updObj.getRequestID());

            DatabaseMetaData dbMeta = cnxn.getMetaData();

            if(dbMeta.supportsTransactionIsolationLevel(Connection.TRANSACTION_SERIALIZABLE)){
                cnxn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

                ps.executeUpdate();

                ResultSet results = ps.getGeneratedKeys();
                while(results.next()) {
                    requestID = results.getInt("request_id");
                }
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown while updating employee record");
            throwables.printStackTrace();
        }
    }
}
