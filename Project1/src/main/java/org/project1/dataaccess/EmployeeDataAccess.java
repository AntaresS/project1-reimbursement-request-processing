package org.project1.dataaccess;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.models.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import io.javalin.http.Context;

/**
 * CRUD operations for a given employee
 */
public class EmployeeDataAccess {
    private DBConnector dbconnect;
    Logger logger = LogManager.getLogger(EmployeeDataAccess.class);
    public EmployeeDataAccess(DBConnector connector) { dbconnect = connector; }

    public List<Employee> getAllEmployees() {
        logger.trace("call to the /api/employees endpoint");
        List<Employee> employeeList = new ArrayList<>();
        //connect to the database
        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "select employee_id, first_name, last_name, street_address, phone_number, employee_role, employee_password, email_address from employee_data";
            //prepare statement to send to the db
            PreparedStatement ps = cnxn.prepareStatement(sql);
            //execute sql statement and set ResultSet equal to the outcome
            ResultSet results = ps.executeQuery();
            //iterate over results
            while(results.next()) {
                Employee emp = new Employee();

                emp.setEmployeeID(results.getInt("employee_id"));
                emp.setFirstName(results.getString("first_name"));
                emp.setLastName(results.getString("last_name"));
                emp.setStreetAddress(results.getString("street_address"));
                emp.setPhoneNumber(results.getString("phone_number"));
                emp.setPosition(results.getString("employee_role"));
                emp.setPassword(results.getString("employee_password"));
                emp.setEmailAddress(results.getString("email_address"));

                employeeList.add(emp);
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown");
            throwables.printStackTrace();
        }
        return employeeList;
    }

    public int saveEmployee(Employee empObject) {
        logger.trace("call to save a new employee");
        int newID = 0;
        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "insert into employee_data(first_name,last_name,street_address,phone_number,employee_role,employee_password,email_address,currently_employed) " +
                        "values (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = cnxn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, empObject.getFirstName());
            ps.setString(2, empObject.getLastName());
            ps.setString(3, empObject.getStreetAddress());
            ps.setString(4,empObject.getPhoneNumber());
            ps.setString(5,empObject.getPosition());
            ps.setString(6,empObject.getPassword());
            ps.setString(7,empObject.getEmailAddress());
            ps.setBoolean(8,empObject.isCurrentEmployee());

            DatabaseMetaData dbMeta = cnxn.getMetaData();

            if(dbMeta.supportsTransactionIsolationLevel(Connection.TRANSACTION_SERIALIZABLE)) {
                cnxn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

                ps.executeUpdate();

                ResultSet results = ps.getGeneratedKeys();
                while(results.next()) {
                    newID = results.getInt("employee_id");

                }
            } else {
                logger.error("employee creation transaction failure");
                throw new RuntimeException("Cannot complete transaction");
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception thrown");
            throwables.printStackTrace();
        }
        return newID;
    }

    public Employee getEmployeeByID(int empID) {
        logger.trace("pulling a single employee from the database");
        Employee emp = new Employee();
        try (Connection cnxn = dbconnect.newConnection()) {
            String sql = "select employee_id, first_name, last_name, street_address, phone_number, employee_role, employee_password, email_address from employee_data where employee_id = ?";
            PreparedStatement ps = cnxn.prepareStatement(sql);
            ps.setInt(1, empID);


            ResultSet results = ps.executeQuery();
            while (results.next()) {

                emp.setEmployeeID(results.getInt("employee_id"));
                emp.setFirstName(results.getString("first_name"));
                emp.setLastName(results.getString("last_name"));
                emp.setStreetAddress(results.getString("street_address"));
                emp.setPhoneNumber(results.getString("phone_number"));
                emp.setPosition(results.getString("employee_role"));
                emp.setPassword(results.getString("employee_password"));
                emp.setEmailAddress(results.getString("email_address"));

            }
        } catch (SQLException throwables) {
            logger.error("SQL exception, most likely NullPointer");
            throwables.printStackTrace();
        }
        System.out.println(emp);
        return emp;
    }

    public int updateEmployee(Employee empObject) {
        logger.trace("changes made to an existing employee record");
        int empID = 0;
        try(Connection cnxn = dbconnect.newConnection()) {
            String sql = "update employee_data set first_name = ?, last_name = ?, street_address = ?, phone_number = ?, email_address = ? where employee_id = ?";
            PreparedStatement ps = cnxn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,empObject.getFirstName());
            ps.setString(2,empObject.getLastName());
            ps.setString(3,empObject.getStreetAddress());
            ps.setString(4,empObject.getPhoneNumber());
            ps.setString(5,empObject.getEmailAddress());
            ps.setInt(6,empObject.getEmployeeID());

            DatabaseMetaData dbMeta = cnxn.getMetaData();

            if(dbMeta.supportsTransactionIsolationLevel(Connection.TRANSACTION_SERIALIZABLE)) {
                cnxn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

                ps.executeUpdate();

                ResultSet results = ps.getGeneratedKeys();
                while(results.next()) {
                    empID = results.getInt("employee_id");
                    System.out.println(empID);
                }
            }
        } catch (SQLException throwables) {
            logger.error("SQL exception, NullPointer or incorrect data type");
            throwables.printStackTrace();
        }
        return empID;
    }
}
