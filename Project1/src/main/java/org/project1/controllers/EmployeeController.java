package org.project1.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.http.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.dataaccess.EmployeeDataAccess;
import org.project1.models.Employee;

import java.util.List;

/**
 * javalin handler for /api/employees routes
 * no returns on any of the methods since they pass the information directly to the browser
 */
public final class EmployeeController {
    private static DBConnector dbconnect = new DBConnector();
    private static EmployeeDataAccess eda = new EmployeeDataAccess(dbconnect);
    private static ObjectMapper objMapper =  new ObjectMapper();
    private static Logger logger = LogManager.getLogger(EmployeeController.class);

    public static void getAllEmployees(Context context) {
        logger.trace("call to getAllEmployees controller");

        List<Employee> empList = eda.getAllEmployees();
        context.json(empList);
    }

    public static void saveEmployee(Context context) {
        System.out.println(context.body());
        try {
            logger.trace("call to save employee method");
            Employee savedEmp = objMapper.readValue(context.body(),Employee.class);
            eda.saveEmployee(savedEmp);
        } catch (JsonProcessingException e) {
            logger.error("Jackson exception processing save employee");
            e.printStackTrace();
        }

        context.status(201);
    }

    public static void getEmployeeByID(Context context) {
        logger.trace("call to getEmployeeByID method");
        Employee emp = eda.getEmployeeByID(Integer.parseInt(context.pathParam("id")));
        context.json(emp);
    }

    public static void updateEmployee(Context context) {
        System.out.println(context.body());
        try{
            Employee emp = objMapper.readValue(context.body(), Employee.class);
            eda.updateEmployee(emp);
            Employee empReturn = eda.getEmployeeByID(emp.getEmployeeID());
            context.json(empReturn);
        } catch (JsonMappingException e) {
            logger.error("Jackson mapping exception updateEmployee method");
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            logger.error("Jackson processing exception updateEmployee method");
            e.printStackTrace();
        }

    }
}
