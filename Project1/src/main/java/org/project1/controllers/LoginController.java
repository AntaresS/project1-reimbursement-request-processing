package org.project1.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.http.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.dataaccess.LoginDataAccess;
import org.project1.models.Login;

import java.io.IOException;

/**
 * Javalin handler method for /api/login route
 * special handler/object combination specifically for logins
 */
public class LoginController {
    private static DBConnector dbconnect = new DBConnector();
    private static LoginDataAccess lda = new LoginDataAccess(dbconnect);
    private static ObjectMapper objMapper = new ObjectMapper();
    private static Logger logger = LogManager.getLogger(LoginController.class);

    public static void pageRedirecton(Context context) {
        Login logReturn = new Login();
        try {
            Login credentials = objMapper.readValue(context.body(),Login.class);

            Login comparison = lda.pageRedirectCredentials(credentials.getEmployeeID(), credentials.getPassword());
            if(comparison.isCurrentEmployee()){
                if(comparison.getPosition().equalsIgnoreCase("manager")){
                    logReturn.setCurrentEmployee(comparison.isCurrentEmployee());
                    logReturn.setEmployeeID(comparison.getEmployeeID());
                    logReturn.setPosition(comparison.getPosition().toLowerCase());
                    context.json(logReturn);
                    context.status(222);
                } else if(comparison.getPosition().equalsIgnoreCase("employee")) {
                    logReturn.setCurrentEmployee(comparison.isCurrentEmployee());
                    logReturn.setEmployeeID(comparison.getEmployeeID());
                    logReturn.setPosition(comparison.getPosition().toLowerCase());
                    context.json(logReturn);
                    context.status(223);
                }
            }
        } catch (JsonProcessingException e) {
            logger.error("JSON processing exception thrown during login sequence");
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("IOexception thrown during login sequence");
            e.printStackTrace();
        }

    }
}
