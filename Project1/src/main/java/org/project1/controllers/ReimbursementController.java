package org.project1.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.http.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.project1.connection.DBConnector;
import org.project1.dataaccess.ReimbursementDataAccess;
import org.project1.models.ReimbursementRequest;
import org.project1.models.UpdateRequestObject;

import java.util.List;

/**
 * Javalin handler methods for the /api/requests routes
 */
public class ReimbursementController {
        private static DBConnector dbconnect = new DBConnector();
        private static ReimbursementDataAccess rda = new ReimbursementDataAccess(dbconnect);
        private static ObjectMapper objMapper = new ObjectMapper();
        private static Logger logger = LogManager.getLogger(ReimbursementController.class);

    public static void getPendingRequests(Context context) {
        List<ReimbursementRequest> pendingReqs = rda.getPendingRequests();
        context.json(pendingReqs);
    }

    public static void getResolvedRequests(Context context) {
        List<ReimbursementRequest> resolvedReqs = rda.getResolvedRequests();
        context.json(resolvedReqs);
    }

    public static void getRequestsByEmployeeID(Context context) {
        List<ReimbursementRequest> empReqs = rda.getRequestsByEmployeeID(Integer.parseInt(context.pathParam("employeeID")));
        context.json(empReqs);
    }

    public static void updateRequestByID(Context context) {
        System.out.println(context.body());
        try {
            UpdateRequestObject upd = objMapper.readValue(context.body(), UpdateRequestObject.class);
            rda.updateRequestByID(upd);
            ReimbursementRequest updatedReq = rda.getRequestByID(upd);
            context.json(updatedReq);
        } catch (JsonProcessingException e) {
            logger.error("JSON exception thrown while updating employee request");
            e.printStackTrace();
        }
    }

    public static void saveRequest(Context context) {
        ReimbursementRequest newReq = null;
        try {
            newReq = objMapper.readValue(context.body(), ReimbursementRequest.class);
        } catch (JsonProcessingException e) {
            logger.error("JSON exception thrown while saving employee request");
            e.printStackTrace();
        }
        int newReqID = rda.saveReimbursementReq(newReq);
        context.json(newReqID);
    }
}
