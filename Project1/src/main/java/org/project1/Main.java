package org.project1;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import org.project1.connection.DBConnector;
import org.project1.controllers.EmployeeController;
import org.project1.controllers.LoginController;
import org.project1.controllers.ReimbursementController;



public class Main {

    public static void main(String[] args) {
        DBConnector dbcconnect = new DBConnector();
        /**
         * start the Javalin server and create the routes and handler methods;
         * the routes can be aggregated based on their paths, see Hibernate&Java project
         */
        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH);
        }).start(7000);

        app.post("/api/login", LoginController::pageRedirecton);

        app.get("/api/employees", EmployeeController::getAllEmployees);
        app.post("/api/employees", EmployeeController::saveEmployee);
        app.get("/api/employees/{id}", EmployeeController::getEmployeeByID);
        app.put("/api/employees/{id}", EmployeeController::updateEmployee);

        app.post("/api/requests", ReimbursementController::saveRequest);
        app.get("/api/requests/pending", ReimbursementController::getPendingRequests);
        app.get("/api/requests/resolved", ReimbursementController::getResolvedRequests);
        app.get("/api/requests/{employeeID}", ReimbursementController::getRequestsByEmployeeID);
        app.put("/api/requests/{requestID}",ReimbursementController::updateRequestByID);
    }
}
