import org.junit.Test;
import static org.junit.Assert.*;
import org.project1.connection.DBConnector;
import org.project1.dataaccess.EmployeeDataAccess;
import org.project1.dataaccess.ReimbursementDataAccess;
import org.project1.models.Employee;
import org.project1.models.Login;
import org.project1.models.ReimbursementRequest;
import org.project1.models.UpdateRequestObject;

import java.util.List;

public class Testing {
    private final DBConnector dbc = new DBConnector();
    EmployeeDataAccess eda = new EmployeeDataAccess(dbc);
    ReimbursementDataAccess rda = new ReimbursementDataAccess(dbc);

//    @Test
//    public void TestTesting() {
//        assertTrue(true);
//    }

    @Test
    public void TestgetAllEmployees() {
        List<Employee> empList= eda.getAllEmployees();
        if(empList.isEmpty()) {
            assertEquals(0,empList.size());
        } else {
            assertNotNull(empList);
        }

    }

    @Test
    public void TestgetPendingRequests() {
        List<ReimbursementRequest> reqList = rda.getPendingRequests();
        if(reqList.isEmpty()) {
            assertEquals(0,reqList.size());
        } else {
            assertNotNull(reqList);
        }
    }

    @Test
    public void TestgetResolvedRequests() {
        List<ReimbursementRequest> reqList = rda.getResolvedRequests();
        if(reqList.isEmpty()) {
            assertEquals(0,reqList.size());
        } else {
            assertNotNull(reqList);
        }
    }

    @Test
    public void TestSaveEmployee_sqlEnforcedUniquePK() {
        Employee dupEmp = new Employee();
        int expected = 12345;
        dupEmp.setEmployeeID(12345);
        dupEmp.setFirstName("Carl");
        dupEmp.setLastName("Weathers");
        int actual = eda.saveEmployee(dupEmp);
        assertNotEquals(expected,actual);
    }

    @Test
    public void TestSaveEmployee_exceedsLowValue() {
        Employee lowEmp = new Employee();
        lowEmp.setEmployeeID(12344);
        lowEmp.setFirstName("Jennifer");
        lowEmp.setLastName("Walters");
        int actual = eda.saveEmployee(lowEmp);
        assertTrue(actual >= 12345);
    }

    @Test
    public void TestSaveRequests_sqlEnforcedUniquePK() {
        ReimbursementRequest dupID = new ReimbursementRequest();
        int expected = 50000;
        dupID.setRequestID(50000);
        dupID.setSubmitterID(99999);
        int actual = rda.saveReimbursementReq(dupID);
        assertNotEquals(expected,actual);
    }

    @Test
    public void TestSaveRequests_exceedsLowValue() {
        ReimbursementRequest lowID = new ReimbursementRequest();
        lowID.setRequestID(49999);
        lowID.setSubmitterID(99999);
        int actual = rda.saveReimbursementReq(lowID);
        assertTrue(actual >= 50000);

    }

    @Test (expected = NullPointerException.class)
    public void TestSaveEmployee_noFirstNameSQLError() {
        Employee noFirstName = new Employee();
        noFirstName.setLastName("JunkDrawer");
        eda.saveEmployee(noFirstName);
        String test = null;
        assertNotEquals(0,test.length());
    }

    @Test(expected = NullPointerException.class)
    public void TestSaveEmployee_noLastNameNullException(){
        Employee noLastName = new Employee();
        noLastName.setFirstName("Joey");
        eda.saveEmployee(noLastName);
        String test = null;
        assertNotEquals(0,test.length());
    }

    @Test(expected = RuntimeException.class)
    public void TestSaveRequest_noSubmitterIDNullException() {
        ReimbursementRequest noSubmitter = new ReimbursementRequest();
        noSubmitter.setAmount(10.25);
        noSubmitter.setDescription("Candy Bars");
        rda.saveReimbursementReq(noSubmitter);
        String test = null;
        System.out.println(test.length());
    }

    @Test
    public void TestEmployeeGetSet() {
        Employee test = new Employee();
        test.setEmployeeID(1);
        assertEquals(1,test.getEmployeeID());
    }
    @Test
    public void TestReimbursementGetSet() {
        ReimbursementRequest test = new ReimbursementRequest();
        test.setAmount(10.25);
        assertEquals(10.25,test.getAmount(),0);
    }

    @Test
    public void TestUpdateGetSet() {
        UpdateRequestObject test = new UpdateRequestObject();
        test.setManagerID(42);
        assertEquals(42,test.getManagerID());
    }
    @Test
    public void TestLoginGetSet() {
        Login test = new Login();
        test.setCurrentEmployee(true);
        assertTrue(test.isCurrentEmployee());
    }

}
